## Raspi Keyboard ##
A basic program in order to emulate a keyboard on a raspi zero w. The basic gist of the program is to read a file from disk and load it to the charcater buffer on the emulated device.


#### Usage ####
Before using the program you will first need to enable the proper modules and drivers in order to allow the raspi to act as a board

     echo "dtoverlay=dwc2" | sudo tee -a /boot/config.txt
     echo "dwc2" | sudo tee -a /etc/modules

If you want support for the led to tell you when the program is running, use this command to disable to the trigger for the led so that the program has absolute control of it

    echo "dtparam=act_led_trigger=none" | sudo tee -a /boot/config.txt
    echo "dtparam=act_led_activelow=on" | sudo tee -a /boot/config.txt

All of the c files only use standard header files that are included in almost all linux operating systems. So this means that your going to have to self compile the source files. Lukely I have made a Makefile that does it all for you. All you have to do is clone the repo then run the make command.

    git clone https://gitlab.com/jmadden173/raspikeyboard.git
    cd raspikeyboard/
    make all

After you should have a keyboard executable in the same directory of the cloned repo along with a setupkeyboard file. The keybaord executable takes two inputs. The first input is the file that is reads to generate the keystrokes. The second is the device buffer usually something like /sys/hid0. You will need to run both of the file on the startup. I would recomend moving them into the /user/local/bin folder and modifying the /etc/rc.local file.

    mv ./keyboard /usr/local/bin/
    mv ./setupkeyboard /usr/local/bin/
    sudo vim /etc/rc.local

Then add the following comnands in the same order

    /usr/local/bin/setupkeyboard
    /usr/local/bin/keyboard $INPUT_FILE $KEYBOARD_BUFFER

#### Commands ####
PRINT - Types out the characters following the space

CNTRL - Holds down control for letter following the space
