char convKeycode(char input)
{	
	/*
	 * TODO
	 * Add All the extra key codes for non alpa numeric keys
	 */
	char output;
	output = input;

	if (input >= 0x61 && input <= 0x7a) // All the normal characters
		output -= 0x5d;

	else if (input >= 0x31 && input <= 0x39) // #'s 1-9
		output -= 0x13;

	else if (input == 0x30) // # 0
		output -= 0x9;

	else if (input == 0x2f) // Forward slash (/)
		output = 0x38;

	else if (input == 0x2e) // dot (.)
		output == 0x37;

	else if (input == 0x0a) // New line (enter)
		output = 0x28;

	else if (input == 0x20) // Space
		output = 0x2c;

	else
		output = 0x00;

	return output;
}


int checkCap(char input)
{
	// The captial range of ascii chart
	if (input >= 0x41 && input <= 0x5a)
		return 1;
	else
		return 0;
}
