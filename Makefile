all: keyboard


keycode: keycode.c
	gcc -c keycode.c keycode.h

error: error.c
	gcc -c error.c error.h


keyboard: keyboard.c error keycode
	gcc -c keyboard.c
	gcc -o keyboard keyboard.o error.o keycode.o


clean:
	rm -f *.o
	rm -f *.gch
	rm -f keyboard

