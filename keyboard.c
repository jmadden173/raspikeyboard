#include <stdio.h>
#include <string.h>
#include "error.h"
#include "keycode.h"

#define KEYBUFFSIZE 8
#define DEBUG 0

#define PRINT "PRINT"
#define SHIFT "SHIFT"
#define CNTRL "CNTRL"


void formatPrint(const char* input, char* buffer, const char* empty, FILE* keyboard);


/*
 * 1st input is the name of the file to read commands from
 * 2nd input is the name of the keyboard buffer file
 */
int main(int argc, char **argv)
{
	// Check if all aguments are provided
	if (argc < 3)
		error("Missing Inputs");


	FILE* led;
	led = fopen("/sys/class/leds/led0/brightness", "wb");

	if (led == NULL)
		printf("Could not open the led\n");
	else
		fwrite("1", 1, 1, led);


	// Create the file stream for the input
	FILE* keyInput;
	keyInput = fopen(argv[1], "rb");
	if (keyInput==NULL)
		error("Could not open input file");


	// Create the file stream for the keyboard
	FILE* keyboard;
	keyboard = fopen(argv[2], "wb");
	if (keyboard==NULL)
		error("Could not open buffer file");


	char inputBuff[1024]; // Create the buffer to load keys into
	memset(inputBuff, 0, sizeof(inputBuff)); // Zero out the buffer


	char keyBuff[KEYBUFFSIZE]; // Init the keyboard buffer

	char emptyBuff[KEYBUFFSIZE]; // Create a empty buffer
	memset(emptyBuff, 0, sizeof(emptyBuff)); // Set to zero


	int line = 0;
	while (fgets(inputBuff, sizeof(inputBuff), keyInput))
	{	
		formatPrint(inputBuff, keyBuff, emptyBuff, keyboard);

		line++; // Log how many lines have be wriiten
	}

	printf("Read %d Lines\n", line);

	// Close all file streams
	fclose(keyInput);
	fclose(keyboard);
	
	if (led != NULL)
		fwrite("0", 1, 1, led);

	fclose(led);

	return 0;
}




void formatPrint(const char* input, char* buffer, const char* empty, FILE* keyboard)
{
	int i = 6;
	while (input[i] != 0x0a)
	{
		// Check the preceding phrase to the ilne
		if (strncmp(input, PRINT, 5) == 0)
		{
			if (checkCap(input[i]))
			{
				buffer[0] = 0x02;
				buffer[2] = convKeycode(input[i] - 0x20); // Switch the capital to lower case
			}
			else
			{
				buffer[0] = 0x00;
				buffer[2] = convKeycode(input[i]); // Set to normal key
			}
		}
		else if (strncmp(input, CNTRL, 5) == 0)
		{
			buffer[0] = 0x01;
			if (checkCap(input[i]))
				buffer[2] = convKeycode(input[i] - 0x20);
			else
				buffer[2] =  convKeycode(input[i]);
		}
		else
			error("Incorrect file format");


		fwrite(buffer, 1, KEYBUFFSIZE, keyboard); // Write it to the keyboard file
		if (DEBUG)
			fwrite("\n", 1, 1, keyboard); // Used for debugging
		else
			rewind(keyboard); // Reset the stream position


		fwrite(empty, 1, KEYBUFFSIZE, keyboard); // Write null to the keyboard file
		if (DEBUG)
			fwrite("\n", 1, 1, keyboard); // Used for debugging
		else
			rewind(keyboard); // Resetthe stream position

		i++;
	}
}
